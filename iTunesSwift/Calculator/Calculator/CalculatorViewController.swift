//
//  ViewController.swift
//  Calculator
//
//  Created by Geddy on 29/11/2016.
//  Copyright © 2016 Smilez. All rights reserved.
//

import UIKit
var calculatorCount = 0
var brain = CalculatorBrain()
var userIsInTheMiddleOfTyping = false

var graphPoints: [Double] = [brain.result]

class CalculatorViewController: UIViewController {
    
    private var on = true
    @IBOutlet private var display: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calculatorCount +=  1
        //print("Loaded up a new Calculator (count = \(calculatorCount))")
        brain.addUnaryOperation(symbol: "Z") { [ weak weakSelf = self ] in
            weakSelf?.display.textColor = UIColor.red
            return sqrt($0)
        }
        
    }
    
    
    deinit {
        calculatorCount -= 1
        //print(" Calculator left the heap (count = \(calculatorCount))")
    }
    
    
    @IBAction func off(_ sender: UIButton) {
        on = false
    }
    
    @IBAction func on(_ sender: UIButton) {
        on = true
    }
    
    @IBAction private func tocuhDigit(_ sender: UIButton) {
        if on {
            let digit = sender.currentTitle!
            if userIsInTheMiddleOfTyping {
                let textCurrentlyInDisplay = display.text!
                display.text =  textCurrentlyInDisplay + digit
            } else {
                display.text = digit
            }
            userIsInTheMiddleOfTyping = true
            
        }
        
        graphPoints.append(Double(sender.currentTitle!)!)
        
    }
    private var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
            
        }
    }
    var savedProgram: CalculatorBrain.PropertyList?
    @IBAction func save() {
        savedProgram = brain.program
        
    }
    
    @IBAction func restore() {
        if savedProgram != nil {
            brain.program = savedProgram!
            displayValue = brain.result
        }
    }
    
    
    
    @IBAction func Reset(_ sender: UIButton) {
        if on {
            
            displayValue = 0
        }
    }
    @IBAction private func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping && on {
            brain.setOperand(operand: displayValue)
            userIsInTheMiddleOfTyping = false
            
            
        }
        
        if let mathematicalSymbol = sender.currentTitle {
            brain.perofrmOperation(symbol: mathematicalSymbol)
            
        }
        displayValue = brain.result
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationvc = segue.destination
        if let navcon = destinationvc as? UINavigationController {
            destinationvc = navcon.visibleViewController ?? destinationvc
        }
        if destinationvc is GraphViewController {
            if segue.identifier != nil {
                //                if let expression = emotionalFaces[identifier] {
                //                    facevc.expression = expression
                //                }
            }
            
        }
        
    }
}
