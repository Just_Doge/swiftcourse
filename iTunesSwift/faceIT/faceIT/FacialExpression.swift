//
//  FacialExpression.swift
//  faceIT
//
//  Created by Geddy on 4/12/2016.
//  Copyright © 2016 Smilez. All rights reserved.
//

import Foundation


struct FacialExpression {
    
    var eyes: Eyes
    var mouth: Mouth
    var eyeBrows: EyeBrows
    
    
    enum Eyes: Int {
        case Open
        case Closed
        case Squinting
    }
    
    enum EyeBrows: Int {
        case Relaxed
        case Normal
        case Furrowed
        
        
        
        
        func moreRelaxedBrow() -> EyeBrows {
            return EyeBrows(rawValue: rawValue - 1) ?? .Relaxed
        }
        func moreFurrowedBrow() -> EyeBrows {
            return EyeBrows(rawValue: rawValue + 1) ?? .Furrowed
            
        }
    }
    
    enum  Mouth: Int {
        case Frown
        case Smirk
        case Neutral
        case Grin
        case Smile
        
        func sadderMouth() -> Mouth {
            return Mouth(rawValue: rawValue - 1) ?? .Frown
        }
        
        func happierMouth() -> Mouth {
            return Mouth(rawValue: rawValue + 1) ?? .Smile
        }
    }
    
}



