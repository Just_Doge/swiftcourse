//
//  EmotionViewController.swift
//  faceIT
//
//  Created by Geddy on 6/12/2016.
//  Copyright © 2016 Smilez. All rights reserved.
//

import UIKit

class EmotionViewController: UIViewController
{
    private let emotionalFaces: Dictionary<String,FacialExpression> = [
        
        "angry" : FacialExpression(eyes: .Open, mouth: .Frown, eyeBrows: .Furrowed),
        "happy" : FacialExpression(eyes: .Open, mouth: .Smile,
                                   eyeBrows: .Relaxed),
        "worried" : FacialExpression(eyes: .Closed, mouth: .Neutral, eyeBrows: .Relaxed),
        "mischievious" : FacialExpression(eyes: .Open, mouth: .Grin, eyeBrows: .Furrowed)
        
    ]
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationvc = segue.destination
        if let navcon = destinationvc as? UINavigationController {
            destinationvc = navcon.visibleViewController ?? destinationvc
        }
        if let facevc = destinationvc as? FaceViewController {
            if let identifier = segue.identifier {
                if let expression = emotionalFaces[identifier] {
                    facevc.expression = expression
                    if let sendingButton = sender as? UIButton {
                        facevc.navigationItem.title = sendingButton.currentTitle
                    }
                }
            }
        }
    }
    
}
