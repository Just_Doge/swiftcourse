//
//  ViewController.swift
//  Flo
//
//  Created by Geddy on 20/12/2016.
//  Copyright © 2016 Smilez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var isGraphViewShowing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        counterLabel?.text = String(counterView.counter)
    }
    
    @IBOutlet weak var counterView: CounterView!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var graphView: GraphView!
    
    @IBOutlet weak var averageWaterDrunk: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnPushButton(button: PushButtonView) {
        if button.isAddButton {
            counterView?.counter += 1
        } else {
            if counterView.counter > 0 {
                counterView.counter -= 1
            }
        }
        counterLabel.text = String(counterView.counter)
        
        if isGraphViewShowing {
            counterViewTap(gesture: nil)
        }
    }
    
    func setupGraphDisplay() {
        
        let noOfDays: Int = 7
        
        graphView.graphPoints[graphView.graphPoints.count - 1] = counterView.counter
        
        graphView.setNeedsDisplay()
        
        maxLabel.text = "\(graphView.graphPoints.max()))"
        
        let average = graphView.graphPoints.reduce(0, +) / graphView.graphPoints.count
        
        averageWaterDrunk.text = "\(average)"
        
        
        
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        let componentOptions: Calendar.Component = .weekday
        let components = calendar.component(Calendar.Component.weekday, from: Date())
        
        var weekday = components
        
        print(weekday)
        
        let days = ["SUN", "M", "T", "W", "TH", "FRI", "SAT"]
        
        for i in (1...days.count).reversed() {
            if let labelView = graphView.viewWithTag(i)
                as? UILabel {
                if weekday == 7 {
                    weekday = 0
                }
                labelView.text = days[weekday - 1]
                if weekday < 0 {
                    weekday = days.count - 1
                }
                
            }}
        
        

        

    }
    
    @IBAction func counterViewTap(gesture:UITapGestureRecognizer?) {
        if (isGraphViewShowing) {
            
            
            //hide Graph
            UIView.transition(from: graphView,
                                      to: counterView,
                                      duration: 1.0,
                                      options: [UIViewAnimationOptions.transitionFlipFromLeft, UIViewAnimationOptions.showHideTransitionViews],
                                      completion: nil)
        } else {
            
            //show Graph
            setupGraphDisplay()
            
            
            UIView.transition(from: counterView,
                              to: graphView,
                              duration: 1.0,
                              options: [UIViewAnimationOptions.transitionFlipFromRight, UIViewAnimationOptions.showHideTransitionViews],
                              completion: nil)
        }
        isGraphViewShowing = !isGraphViewShowing
    }
    
    
}

