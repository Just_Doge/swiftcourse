//
//  ViewController.swift
//  Calculator2
//
//  Created by Geddy on 30/12/2016.
//  Copyright © 2016 Smilez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var display: UILabel!
    
    private var userIsInTheMiddleOfTyping = false
    
    
    @IBOutlet weak var ListOfOperations: UILabel!
    
    @IBAction private func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            display.text = textCurrentlyInDisplay + digit
            brain.description += brain.description
            ListOfOperations.text = brain.description
            
        }
        else {
            display.text = digit
        }
        userIsInTheMiddleOfTyping = true
        
    }
    
    private var displayValue: Double {
        get {
           return Double(display.text!)!
        }
        set {
           display.text = String(newValue)
        }
    }
    private var brain = CalculatorBrain()
    
    @IBAction private func performOperation(_ sender: UIButton) {
        if brain.isPartialResult {
            ListOfOperations.text = brain.description
            print(ListOfOperations.text)
        }
        if userIsInTheMiddleOfTyping {
            brain.setOperand(operand: (displayValue))
            userIsInTheMiddleOfTyping = false
        }
        
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(symbol: mathematicalSymbol)
        }
        displayValue = brain.result
    }
    
}

