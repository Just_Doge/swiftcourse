//
//  CalculatorBrain.swift
//  Calculator2
//
//  Created by Geddy on 31/12/2016.
//  Copyright © 2016 Smilez. All rights reserved.
//

import Foundation

func sqr(op1: Double) -> Double {
    return op1 * op1
}

class CalculatorBrain {
    
    private var accumulator = 0.0
    var description = ""
    
    func setOperand(operand: Double) {
        accumulator = operand
    }
    var isPartialResult = false
    
    
    var operations: Dictionary<String, Operation> = [
        "π" : Operation.Constant(M_PI),
        "e" : Operation.Constant(M_E),
        "±" : Operation.UnaryOperation({ -$0 }),
        "√" : Operation.UnaryOperation(sqrt),
        "cos" : Operation.UnaryOperation(cos),
        "×" : Operation.BinaryOperation({$0 * $1 }),
        "+" : Operation.BinaryOperation({$0 + $1 }),
        "−" : Operation.BinaryOperation({$0 - $1 }),
        "÷" : Operation.BinaryOperation({$0 / $1 }),
        "%" : Operation.BinaryOperation({$0 * 100 / $1 }),
        "=" : Operation.Equals,
        "x2" : Operation.UnaryOperation(sqr),
        "xy" : Operation.BinaryOperation(pow)
        
    ]
    
    enum Operation {
        case Constant(Double)
        case UnaryOperation((Double) -> Double)
        case BinaryOperation((Double, Double) -> Double)
        case Equals
    }
    func performOperation(symbol: String) {
        
        if symbol != "=" {
            description += symbol
        }
        if let operation = operations[symbol] {
            switch operation {
            case .Constant(let value): accumulator = value
            case .UnaryOperation(let function): accumulator = function(accumulator)
            case .BinaryOperation(let function):
                executePendingBinaryOperation()
                pending = PendingBindingOperationInfo(binaryFunction: function, firstOperand: accumulator)
            case .Equals: executePendingBinaryOperation()
                
                
            }
        }
        
    }
    
    private func executePendingBinaryOperation() -> Bool {
        if pending != nil {
            accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
            pending = nil
            isPartialResult = true
        } else {
            isPartialResult = false
        }
        
        return isPartialResult
    }
    
    
    private var pending: PendingBindingOperationInfo?
    
    struct PendingBindingOperationInfo {
        var binaryFunction: (Double, Double) -> Double
        var  firstOperand: Double
        
    }
    
    var result: Double {
        get {
            return accumulator
        }
    }
    
}
