//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

struct test {
    
    enum test2: Int {
    case fer
    case foo
    case wow
    }
    enum test3: Int {
        case ref
        case wow
        case oof
        case doof
    }
    
    enum test4: Double {
        case thing
        case erf
        case ofo
        case wwo
        case food
    }
}


var newStruct = test()
